% Ritco Atlas
% Term Project - Asteroid Field
%                Description: Space obstacle game.
%                Music Credits: 
%                   "Asteroid Field" by Waveshaper, 2015
%                   Game Over, Supuer Mario Bros., Nintendo

function [] = AsteroidField()
clc,clear;
gameOver = false;
gameStarted = false;
highScore = fileread('highScoreLog.txt');

%--- VARIABLES ------------------
score = 0;
scoreDisplay = [];
h = [];
asteroid = [];
endText = [];
endText2 = [];
restartButton = [];
highScoreDisp = [];
newHighScoreText = [];
titleText = [];
startButton = [];
incomingText = [];
hintText = [];
player = [];
crashPlayer = [];

%--- SPRITE POSITIONS -----------
global CURRENTY;
CURRENTY = 220;
shipPosition = [20 CURRENTY 122 52];

global ASTEROID_X;
ASTEROID_X = 850; % 850 to -100
global ASTEROID_Y;
ASTEROID_Y = 175; %between 0 and 350
asteroidPosition = [ASTEROID_X ASTEROID_Y 100 100];

%------------------------------SET UP--------------------------------------
function createFigure
    %--- FIGURE AND BACKGROUND ----------------------------
    gameFigure = figure('Name','Asteroid Field',...
        'Numbertitle','off',...
        'Position',[400 200 800 500],...
        'Menubar','none',...
        'Resize','off',...
        'Color','black',...
        'KeyPressFcn',@keyPressFcn);
    
    ha = axes('units','normalized',...
        'position',[0 0 1 1]);
    uistack(ha,'bottom');
    spaceImg = imread('space.png');
    hi = imagesc(spaceImg);
    colormap gray
    set(ha,'handlevisibility','off',...
        'visible','off')

    %--- SCORES -------------------------------------------- 
    scoreDisplay = uicontrol(gameFigure,'Style','text',...
        'Position',[695 460 100 34],...
        'ForegroundColor','white',...
        'BackgroundColor','black',...
        'FontSize',20);

    highScoreDisp = uicontrol(gameFigure,'Style','text',...
        'Position', [70 459 150 30],...
        'ForegroundColor','white',...
        'BackgroundColor','none',...
        'FontSize',10);
    highScoreTxt = 'HIGH SCORE: ';
    set(highScoreDisp,'String',strcat(highScoreTxt,highScore));
    set(scoreDisplay,'String',score);

    %--- MUSIC -----------------------------------------------
    [s,Fs] = audioread('asteroidfieldmusic.mp3');
    player = audioplayer(s,Fs);
    
    % Crash Music
    [t,Ft] = audioread('dieSound.mp3');
    crashPlayer = audioplayer(t,Ft);
    
    % ---- SPRITES ---------------------------------------------
    shipImg = imread('ship.png');
    I2 = imresize(shipImg, [52 125]);
    h = uicontrol(gameFigure,...
        'style','pushbutton',...
        'units','pixels',...
        'position',shipPosition,...
        'cdata',I2);
    
    asteroidImg = imread('asteroid.png');
    I3 = imresize(asteroidImg, [100 100]);
    asteroid = uicontrol(gameFigure,...
        'style','pushbutton',...
        'units','pixels',...
        'position',asteroidPosition,...
        'cdata',I3);
    
    %--- START SCREEN ------------------------------------------
    titleText = uicontrol(gameFigure,'Style','text',...
        'Position',[225 170 350 120],...
        'ForegroundColor','white',...
        'BackgroundColor',[0 0.4470 0.7410],...
        'FontName','Agency FB',...
        'String','ASTEROID FIELD',...
        'Visible','on',...
        'FontSize',45);
    startButton = uicontrol(gameFigure,...
        'Position', [365 185 70 30],...
        'String', 'Start',...
        'ForegroundColor','white',...
        'BackgroundColor',[.3 .3 .3],...
        'Visible','on',...
        'FontSize',12);
    startButton.Callback = @startButtonPushed;
     
    incomingText = uicontrol(gameFigure,'Style','text',...
        'Position',[500 -40 300 100],...
        'ForegroundColor','white',...
        'BackgroundColor','black',...
        'String','INCOMING ASTEROIDS!',...
        'Visible','off',...
        'FontSize',15);
    hintText = uicontrol(gameFigure,'Style','text',...
        'Position',[495 -65 300 100],...
        'ForegroundColor','white',...
        'BackgroundColor','black',...
        'String','Click the screen to take control of the ship!',...
        'Visible','off',...
        'FontSize',10);
        % Exit Button
    quitButton = uicontrol(gameFigure,...
        'Position', [10 465 70 30],...
        'String', 'Quit',...
        'ForegroundColor','white',...
        'BackgroundColor',[.3 .3 .3],...
        'FontSize',12);
    quitButton.Callback = @quitButtonPushed;
    
    
    %-----------End Screen------------------------------------------------
    endText = uicontrol(gameFigure,'Style','text',...
        'Position',[150 150 500 200],...
        'ForegroundColor','white',...
        'BackgroundColor',[0.6350 0.0780 0.1840],...
        'FontName','Agency FB',...
        'String','GAME OVER',...
        'Visible','off',...
        'FontSize',50);
    endText2 = uicontrol(gameFigure,'Style','text',...
        'Position',[150 150 500 120],...
        'ForegroundColor','white',...
        'BackgroundColor',[0.6350 0.0780 0.1840],...
        'String','Your ship crashed into an asteroid!',...
        'Visible','off',...
        'FontSize',15);
    newHighScoreText = uicontrol(gameFigure,'Style','text',...
        'Position',[150 150 500 95],...
        'ForegroundColor','white',...
        'BackgroundColor',[0.6350 0.0780 0.1840],...
        'String','New high score!',...
        'Visible','off',...
        'FontSize',15);
    restartButton = uicontrol(gameFigure,...
        'Position', [365 175 70 30],...
        'String', 'Restart',...
        'ForegroundColor','white',...
        'BackgroundColor',[.3 .3 .3],...
        'Visible','off',...
        'FontSize',12);
    restartButton.Callback = @restartButtonPushed;
end

% ------------------ BUTTON/KEYPRESS CALLBACKS ----------------------------
function restartButtonPushed(src,event)
newGame;
end

function startButtonPushed(src,event)
gameStarted = true;
set(titleText,'visible','off');
set(startButton,'visible','off');
end

function keyPressFcn(src,event)
    if(gameOver == false)
        if(CURRENTY == 3)
            CURRENTY = CURRENTY+7;
        elseif(CURRENTY == 395)
            CURRENTY = CURRENTY-7;
        end
        if(CURRENTY<=388 && CURRENTY>=10)
            switch event.Key
                case 'uparrow'
                    newY = CURRENTY+7;
                    newPosition = [20 newY 123 52];
                    set(h,'position',newPosition);
                    CURRENTY = newY;
                case 'downarrow'
                    newY = CURRENTY-7;
                    newPosition = [20 newY 123 52];
                    set(h,'position',newPosition);
                    CURRENTY = newY;
            end
        end
    end
end

%--------------------------- NEW GAME -------------------------------------
function newGame
    pause(crashPlayer);
    resume(player);
    score = 0;
    set(scoreDisplay,'String',floor(score));
    gameOver = false;
    set(asteroid,'position',[850 175 100 100]);
    set(endText,'visible','off');
    set(endText2,'visible','off');
    set(restartButton,'visible','off');
    set(newHighScoreText,'visible','off');
    
    set(incomingText,'visible','on');
    set(hintText,'visible','on');
    pause(2);
    set(incomingText,'visible','off');
    set(hintText,'visible','off');
    newAsteroid();
    
    while (gameOver == false)
        drawnow;
        collisionDetected = checkCollision();
        if(collisionDetected == true)
            gameOver = true;
            pause(player);
            play(crashPlayer);
        else
            score = updateScore(collisionDetected,score);
            set(scoreDisplay,'String',floor(score));
        end
    
        if(ASTEROID_X < -100)
            newAsteroid();
        else
            asteroidPosition = moveAsteroid();
            set(asteroid,'position',asteroidPosition);
        end
    end
set(endText,'visible','on');
set(endText2,'visible','on');
set(restartButton,'visible','on');

if(score>highScore)
    highScore = score;
    chr = int2str(highScore-1);
    highScoreFile = fopen('highScoreLog.txt','wt');
    fprintf(highScoreFile,chr);
    fclose(highScoreFile);
    set(highScoreDisp,'String',strcat('HIGH SCORE: ',chr));
    set(newHighScoreText,'visible','on');
end
end


%---------------------------- MAIN/LOOP -----------------------------------
createFigure;
while(gameStarted == false)
    drawnow;
end

newGame;

end