function [] = newAsteroid
    global ASTEROID_X;
    global ASTEROID_Y;
    ASTEROID_X = 850;
    r = randi([0,350]);
    ASTEROID_Y = r;
end