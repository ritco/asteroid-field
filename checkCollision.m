function [collisionDetected] = checkCollision()
    global CURRENTY;
    global ASTEROID_Y;
    global ASTEROID_X;
    asY = ASTEROID_Y;
    asX = ASTEROID_X;
    shipY = CURRENTY;
    
    if((((shipY+36)>asY && (shipY<asY)) || ((asY+70)>shipY && asY<shipY)) && asX == 115)
        collisionDetected = true;
    else
        collisionDetected = false;
    end
end